CONTRIBUTING
============

### Localizations:

Fedilab works only with [Weblate]https://hosted.weblate.org/projects/fedilab), which offers nice
tools for helping in translations. New translations will be automatically merged in a branch.

If you're submiting a merge request and your work adds new strings to the app, make sure they only
exist in the default strings.xml file (res/values/strings.xml). If you add or modify strings of
other languages, it will interfere with weblate's translations.